import boto.vpc
import boto.ec2
import boto.ec2.elb

aws_access_key_id = ''
aws_secret_access_key = ''
vpc_id = ''

#show = []
show = ['lb', 'sg', 'rt', 'acl']

#sort = 'id'
sort = 'name'

table = {'4': '268435454', '5': '134217726', '6': '67108862', '7': '33554430', '8': '16777214', '9': '8388606', '10': '4194302', '11': '2097150', '12': '1048574', '13': '524286', '14': '262142', '15': '131070', '16': '65534', '17': '32766', '18': '16382', '19': '8190', '20': '4094', '21': '2046', '22': '1022', '23': '510', '24': '254', '25': '126', '26': '62', '27': '30', '28': '14', '29': '6', '30': '2', '31': '0', '32': '1'}


print "connecting to vpc"
vpc_conn = boto.vpc.connect_to_region('ap-southeast-2', aws_access_key_id=aws_access_key_id, aws_secret_access_key=aws_secret_access_key)

print "connecting to ec2"
ec2_conn = boto.ec2.connect_to_region('ap-southeast-2', aws_access_key_id=aws_access_key_id, aws_secret_access_key=aws_secret_access_key)

print "connecting to elb"
elb_conn = boto.ec2.elb.connect_to_region('ap-southeast-2', aws_access_key_id=aws_access_key_id, aws_secret_access_key=aws_secret_access_key)

print "loading vpc"
vpc = vpc_conn.get_all_vpcs([vpc_id])

print "loading subnet"
subnets = vpc_conn.get_all_subnets(filters=[('vpcId', vpc_id)])

print "loading securety group"
sgs = vpc_conn.get_all_security_groups(filters={'vpc-id': vpc_id})

print "loading route table"
rts = vpc_conn.get_all_route_tables(filters={'vpc-id': vpc_id})

print "loading acls"
acls = vpc_conn.get_all_network_acls(filters={'vpc-id': vpc_id})

print "loading loadbalancer"
elbs = elb_conn.get_all_load_balancers()

print "\n+", vpc[0].id, '\t', vpc[0].state, '\t', vpc[0].cidr_block, '\t', vpc[0].tags['Name'] if 'Name' in vpc[0].tags else "", "\n"

if 'lb' in show:

    print "\n    - loadbalancers:\n"
    for elb in sorted(elbs, key=lambda x: x.name):
        if elb.vpc_id == vpc_id:
            print "        -", elb.name, "\t", elb.availability_zones, "\t", elb.subnets, "\t", elb.backends, "\t", elb.security_groups
    	    print "\n            - instance:\n"
    
    
    	    for ii in elb.instances:
    	        print "                -", ii.id, "\t", ii.state
    
            print "\n"
    





print "\n    + subnet:\n"
for s in subnets:

    instances = ec2_conn.get_all_instances(filters={'vpc-id': vpc_id, 'subnet-id': s.id})
    print "\n        +", s.id, "\t", s.state, "\t", s.cidr_block, "(%d/%s)" % (len(instances), table[s.cidr_block[s.cidr_block.index('/')+1:]]), "\t", s.availability_zone, "\n"

    if 'rt' in show:

        print "            - route tables:\n"
    
        for rt in rts:
            if rt.id is None:
                continue
            for rta in rt.associations:
                if rta.subnet_id == s.id:
                    print "                -", rt.id, "\t", rt.tags['Name'] if 'Name' in rt.tags else "", "\n"
    
                    for r in rt.routes:
    	                print "                    -", r.state, "\t", r.destination_cidr_block
    
                    print "\n"
    		    break


    if 'acl' in show:

        print "            - network acls:\n"
        for acl in acls:
    	    for ac in acl.associations:
    	        if ac.subnet_id == s.id:
                    print "                -", acl.id, "\n"
    
                    for ae in acl.network_acl_entries:
                        if ae.egress == 'true':
                            print "                    - outbound: ", ae.protocol, "\t", ae.rule_action, "\t", ae.cidr_block, "\t", ae.port_range, "\t", ae.rule_number
                        else:
                            print "                    - inbound: ", ae.protocol, "\t", ae.rule_action, "\t", ae.cidr_block, "\t", ae.port_range, "\t", ae.rule_number

    
                    print "\n"
                    break

    print "            - instance:\n"
    for i in sorted(instances, key=lambda x: x.instances[0].tags['Name'] if 'Name' in x.instances[0].tags else "" if sort == 'name' else getattr(x.instances[0], sort)):
        for ii in i.instances:
	    name = ""
	    if 'Name' in ii.tags:
		name = ii.tags['Name']
	    else:
		name = ""

	    print "                -", ii.id, "\t", ii.state, "\t", ii.private_ip_address, "\t", ii.ip_address, "\t", name

            if 'sg' in show:

                print "\n                    - securety groups:\n"
	        for iig in ii.groups:

                    for sg in sgs:
                        if iig.id == sg.id:
                            print "                        -", sg.id, "\t", sg.name, "\t", sg.description, "\n"
                    
                            for r in sg.rules:
                                for g in r.grants:
				    if g.cidr_ip == None:
                                        print "                            - inbound: ", r.ip_protocol, "\t", r.from_port, "\t", r.to_port, "\t", g.group_id
				    else:
                                        print "                            - inbound: ", r.ip_protocol, "\t", r.from_port, "\t", r.to_port, "\t", g.cidr_ip

                            for r in sg.rules_egress:
                                for g in r.grants:
				    if g.cidr_ip == None:
                                        print "                            - outbound: ", r.ip_protocol, "\t", r.from_port, "\t", r.to_port, "\t", g.group_id
				    else:
                                        print "                            - outbound: ", r.ip_protocol, "\t", r.from_port, "\t", r.to_port, "\t", g.cidr_ip
                    
                    
                            print "\n"
                            break


print "-----------------------------"
